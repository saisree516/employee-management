import { EmployeeData } from "../dbutils/model.js";

/**
 * This function is used to retrive the employee details.
 * @param {*} req is used to get the employeedetails from the server
 * @param {*} res sends statuscode 200 if the data is retrieved successfully
 * else we get the error message
 */
const get_employee_details_handler = async (req, res) => { 
    try {
        const emp_data = await EmployeeData.find();
                
        res.status(200).json(emp_data);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export { get_employee_details_handler }