// importing EmployeeData
import { EmployeeData } from "../dbutils/model.js";

/**
 * This function is used to post the employee details.
 * @param {*} req by using this method we can post the employee details into the database 
 * @param {*} res sends statuscode 200 if the data is posted successfully
 * else we get the error message as Unable to create employee, server error
 */
const Post_Employee_details_handler = (req, res) => {
  const { FirstName, LastName, Email, Phoneno, Department, Project, Address } =
    req.body;

  const employee_data = {
    FirstName,
    LastName,
    Email,
    Phoneno,
    Department,
    Project,
    Address,
  };
  const new_employee = new EmployeeData(employee_data);
  new_employee.save((err, result) => {
    if (err) {
      res.status(500).send("Unable to create employee, server error! " + err);
    } else {
      res.send({ msg: "Posted Employee details successfully" });
    }
  });
};

export { Post_Employee_details_handler };
