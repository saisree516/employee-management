import { EmployeeData } from "../dbutils/model.js"


/**
 * This function is used to delete the employee details.
 * @param {*} req this method is to delete the records based on the firstname
 * @param {*} res sends statuscode 200 if the data is deleted successfully
 * else we get the error message
 */
const delete_employee_handler=async( req, res) =>{
    try{
    const data=await EmployeeData.deleteOne({FirstName: req.body.FirstName})
    res.status(200).json(data);
     } catch(error){
        console.log(error)
        res.status(404).json({ message: error.message });
    }
}
export { delete_employee_handler }
