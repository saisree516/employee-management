import { EmployeeData } from "../dbutils/model.js"

/**
 * This function is used to update the employee details.
 * @param {*} req by using this method we can update the employee project based 
 * on the department into the database 
 * @param {*} res sends statuscode 200 if the data is updated successfully
 * else we get the error message
 */
const update_employee_handler=async( req, res) =>{
    try{
    const data=await EmployeeData.updateOne({Project: req.body.Project})
    res.status(200).json(data);
     } catch(error){
        console.log(error)
        res.status(404).json({ message: error.message });
    }
}
export { update_employee_handler }
