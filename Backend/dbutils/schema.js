// importing mongoose
import mongoose from "mongoose";

/**
 * This is the definition of the schema to be used to convert into a model
 * for the MongoDB implementation.
 * This model represents the Employee Details
 * on the application.
 */
const EmployeeSchema=new mongoose.Schema({
    FirstName:    {type:String, required:true},
    LastName:     {type:String, required:true},
    Email:        {type:String, required:true},
    Phoneno:      {type:Number, required:true},
    Department:   {type:String, required:true},
    Project:      {type:String, required:true},
    Address:      {type:String}
})

export { EmployeeSchema }