import {
  _firstname_helper,
  _lastname_helper,
  _email_helper,
  _phoneNo_helper,
  _department_helper,
  _project_helper,
  _address_helper,
} from "../helpers/helpers.js";

/**
 * This is a helper middleware function to help with validation of employee details
 * parameters.
 * @param {}  req   HTTP req object as recieved by Express backend
 * @param {*} res   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 * @returns
 */
const employee_data_validator = (req, res, next) => {
  const { FirstName, LastName, Email, Phoneno, Department, Project, Address } =
    req.body;
  //  register_keys_validator(req.body,response)
  const error_object = {};
  error_object.FirstName = _firstname_helper(FirstName).FirstName;
  error_object.LastName = _lastname_helper(LastName).LastName;
  error_object.Email = _email_helper(Email).Email;
  error_object.Phoneno = _phoneNo_helper(Phoneno).Phoneno;
  error_object.Department = _department_helper(Department).Department;
  error_object.Project = _project_helper(Project).Project;
  error_object.Address = _address_helper(Address).Address;
  for (const [key, value] of Object.entries(error_object)) {
    if (value) {
      return res.status(406).send(error_object);
    }
  }

  next();
};

export { employee_data_validator };
