/**
 * Helper function to validate firstname string
 * checks if name is string, else returns "firstname should be of string type"
 * checks if name is too short, else returns "firstname should be at least 4 characters long and max of 25"
 * checks if name is of the correct format, else returns "firstname should not have at least 1 special character and at least 1 digit"
 * @param {} FirstName
 * @returns
 */
const _firstname_helper = (FirstName) => {
  const returnObject = {};

  if (typeof FirstName != "string") {
    returnObject.FirstName = "firstname should be of string type";
    return returnObject;
  }

  if (FirstName.length < 3) {
    returnObject.FirstName =
      "firstname length should be at least 4 characters long";
    return returnObject;
  }

  if (FirstName.length > 25) {
    returnObject.FirstName = "firstname length shouldn't be 25 characters long";
    return returnObject;
  }

  if (!FirstName.match(/^[a-zA-Z ]+$/)) {
    returnObject.FirstName = "firstname should be of the correct format";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate lastname laststring
 * checks if lastname is string, else returns "lastname should be of string type"
 * checks if lastname is too short, else returns "lastname should be at least 4 characters long and max of 25"
 * checks if lastname is of the correct format, else returns "lastname should not have at least 1 special character and at least 1 digit"
 * @param {} LastName
 * @returns
 */
const _lastname_helper = (LastName) => {
  const returnObject = {};

  if (typeof LastName != "string") {
    returnObject.LastName = "lastname should be of string type";
    return returnObject;
  }

  if (LastName.length < 3) {
    returnObject.LastName =
      "lastname length should be at least 4 characters long";
    return returnObject;
  }

  if (LastName.length > 25) {
    returnObject.LastName = "lastname length shouldn't be 25 characters long";
    return returnObject;
  }

  if (!LastName.match(/^[a-zA-Z ]+$/)) {
    returnObject.LastName = "lastname should be of the correct format";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate email string
 * checks if email is string, else returns "email should be of string type"
 * checks if email is too short, else returns "email should be at least 6 characters long and max of 50"
 * checks if email is of the correct format, else returns "email should be of the correct format" 406
 * @param {} Email
 * @returns
 */
const _email_helper = (Email) => {
  const returnObject = {};
  // is it a string
  if (typeof Email != "string") {
    returnObject.Email = "email should be of string type";
    return returnObject;
  }
  if (Email.length < 6) {
    returnObject.Email = "email length should be at least 6 characters long";
    return returnObject;
  }
  if (Email.length > 50) {
    returnObject.Email = "email length should be a max of 50";
    return returnObject;
  }
  if (
    !Email.match(
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
  ) {
    returnObject.Email = "email should be of the correct format";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate phone number
 * checks if phone no is number, else returns "phone number should be of number type"
 * checks if phone no is too short, else returns "phone number should be at least 1 characters long and max of 10"
 * checks if phone no is of the correct format, else returns "phone number should be of the correct format" 406
 * @param {} phoneNo
 * @returns
 */
const _phoneNo_helper = (Phoneno) => {
  const returnObject = {};

  if (typeof Phoneno != "number") {
    returnObject.Phoneno = "phone number should be of number type";
    return returnObject;
  }

  if (Phoneno.toString().length < 10) {
    returnObject.Phoneno =
      "phone number length should be at least 10 characters long";
    return returnObject;
  }

  if (Phoneno.length > 10) {
    returnObject.Phoneno = "phone number length should be a max of 10";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate department string
 * checks if department is string, else returns "address should be of string type"
 * @param {} Deparment
 * @returns
 */
const _department_helper = (Department) => {
  const returnObject = {};
  if (typeof Department != "string") {
    returnObject.Department = "department should be of string type";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate department string
 * checks if project is string, else returns "address should be of string type"
 * @param {} Project
 * @returns
 */
const _project_helper = (Project) => {
  const returnObject = {};
  if (typeof Project != "string") {
    returnObject.Project = "project should be of string type";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate address string
 * checks if address is string, else returns "address should be of string type"
 * @param {} address
 * @returns
 */
const _address_helper = (Address) => {
  const returnObject = {};
  if (typeof Address != "string") {
    returnObject.Address = "address should be of string type";
    return returnObject;
  }
  return returnObject;
};

const employee_keys_validator = (user_data, response, next) => {
  let body_keys_list = [];

  for (let item in user_data.body) {
    body_keys_list.push(item);
  }

  //delaring a list of keys to validate the keys
  let key_list = [
    "FirstName",

    "LastName",

    "Email",

    "Phoneno",

    "Department",

    "Project",

    "Address",
  ];

  for (let key of key_list) {
    if (body_keys_list.includes(key)) {
      continue;
    } else {
      let msg = key + " is Missing!";

      return response
        .status(409)
        .send(JSON.stringify({ status_code: 406, status_message: msg }));
    }
  }
  next();
};

export {
  _firstname_helper,
  _lastname_helper,
  _email_helper,
  _phoneNo_helper,
  _department_helper,
  _project_helper,
  _address_helper,
  employee_keys_validator,
};
