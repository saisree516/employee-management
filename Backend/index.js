import express from "express";

//internal dependencies
import { get_employee_details_handler } from "./path_handlers/get_api.js";
import { Post_Employee_details_handler } from "./path_handlers/post_api.js";
import { update_employee_handler } from "./path_handlers/update_api.js";
import { delete_employee_handler } from "./path_handlers/delete_api.js";
import { employee_data_validator } from "./validations/employeevalidations.js";

// creating an instance of express
const app=express();

app.use(express.json())

// api call to post the employee details
app.post("/employeedetails", employee_data_validator, Post_Employee_details_handler)

// api call to retrive the employee details
app.get("/getempdetails", get_employee_details_handler)

// api call to update the employee project details based on department
app.put("/update/:Department", update_employee_handler)

// api call to delete the employee details based on firstname
app.delete("/delete/:FirstName", delete_employee_handler)

// app.listen() function is used to bind and listen the connections on the specified host and port.
app.listen(5000, () => {
    console.log("listening to port 5000");
});
  